const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10
const mysql = require('mysql')

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'DormSystem'
})

connection.connect()

const express = require('express')
const { response } = require('express')
const app = express()
const port = 4000


/* Middleware for Authenticating User Toke */

function authenticateToken(req,res,next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err,user) => {
        if(err) 

        {   console.log(err)
            return res.sendStatus(403)}
        else{
            req.user = user
            next()
        }
    })
}


/*API for registering a new ternant*/

app.post("/login" ,(req,res) => {

    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Tenant WHERE Username='${username}'`
    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json( {
                    "status" : "400",
                    "message" : "Error inserting data into db"
                    })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(user_password,db_password, (err,result) =>{
                if (result){
                   let payload = {
                       "username" : rows[0].Username,
                       "user_id" : rows[0].RunnerID,
                   }
                   console.log(payload)
                   let token = jwt.sign(payload,TOKEN_SECRET,{expiresIn : '1d'})
                   res.send(token)
                }else{res.send("Invalid username / password")}
            })
        }

    })
})
app.post("/register_tenant" , (req,res) => {

    let tenant_name = req.query.tenant_name
    let tenant_surname = req.query.tenant_surname
    let tenant_username = req.query.tenant_username
    let tenant_password = req.query.tenant_password

    bcrypt.hash(tenant_password, SALT_ROUNDS , (err,hash) =>{
            
            let query = `INSERT INTO Tenant
                         (TenantName,TenantSurname,Username,Password) 
                         VALUES('${tenant_name}','${tenant_surname}',
                        '${tenant_username}','${hash}')`

            console.log(query)

            connection.query( query, (err, rows) => {
                if(err){
                    console.log(err)
                    res.json( {
                            "status" : "400",
                            "message" : "Error inserting data into db"
                            })
                }else{
                    res.json( {
                        "status" : "200",
                        "message" : "Adding new user successful"
                        })
                    }
                })  
            })

})


/* ----------------- CRUD Operation -----------------------*/

/* ADD RESERVATION (INSERT INTO)*/

app.post ("/add_reserve" ,(req ,res) =>{

    let ternant_id = req.query.ternant_id
    let room_id = req.query.room_id

    let query = `INSERT INTO Reservation
                    (TenantID,RoomID,ReservationTime) 
                    VALUES(${ternant_id},${room_id},NOW() )`

    console.log(query)

    connection.query( query, (err, rows) => {
        if(err){
            console.log(err)
            res.json( {
                    "status" : "400",
                    "message" : "Error can't adding reservation"
                    })
        }else{
            res.json( {
                "status" : "200",
                "message" : "Adding reserve successful"
                })
            }
        })
})

/* LIST RESERVATION (SELECT) */

app.get("/list_reserve", (req,res) => {
    let query = "SELECT * FROM Reservation";
    connection.query( query, (err, rows) => {
    if(err){
        console.log(err)
        res.json( {
                "status" : "400",
                "message" : "Error querying from running db"
                })
    }else{
        res.json(rows);
        }
    })
})

/* UPDATE ROOM STATUS  (UPDATE)*/

app.post ("/update_room",authenticateToken ,(req,res) =>{

    let room_number = req.query.room_number
    let room_status = req.query.room_status

    let query = `UPDATE Room SET                 
                RoomStatus ='${room_status}'   
                WHERE RoomNumber='${room_number}' `
    
    console.log(query)

    connection.query( query, (err, rows) => {
        if(err){
            console.log(err)
            res.json( {
                    "status" : "400",
                    "message" : "Error Updating record"
                    })
        }else{
            res.json( {
                "status" : "200",
                "message" : "Updating room successful"
                })
            }
        })
})


/* DELETE RESERVATION (DELETE) */

app.post ("/delete_reserve",authenticateToken,(req,res) =>{

    let reserve_id = req.query.reserve_id

    let query = `DELETE FROM Reservation WHERE ReservationID=${reserve_id} `
    
    console.log(query)

    connection.query( query, (err, rows) => {
        if(err){
            console.log(err)
            res.json( {
                    "status" : "400",
                    "message" : "Error deleting record"
                    })
        }else{
            res.json( {
                "status" : "200",
                "message" : "Deleting record successful"
                })
            }
        })
})

app.listen(port, () => {
    console.log(`Now starting Dorm System Backend ${port}`)
})